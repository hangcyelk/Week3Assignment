
def solve_puzzler():
	fin = open('words.txt')
	puzzler = []
	for line in fin:
		word = line.strip()
		count = 0
		if len(word) >= 5:
			for i in range(len(word)-5):
				if word[i] == word[i+1] and word[i+2] == word[i+3] and word[i+4] == word[i+5]:
					count += 1
				else:
					continue
			if count == 1:
				puzzler.append(word)
			else:
				continue
		else:
			continue
	return puzzler

print (solve_puzzler())




