def rotate_word(word,rot):
	rotate = ''
	for c in word:
		loc = ord(c)
		if loc >= 97 and loc <= 122:
			des = loc + rot
			if des > 122:
				des = 96+(des-122)
				c = chr(des) 
			elif des < 97:
				des = 123 - (97-des)
				c = chr(des)
			else:
				c = chr(des)
		elif loc >= 65 and loc <= 90:
			des = loc + rot 
			if des > 90:
				des = 64+(des-90)
				c = chr(des) 
			elif des < 65:
				des = 91 - (65-des)
				c = chr(des)
			else:
				c = chr(des)
		rotate += c
	return rotate

