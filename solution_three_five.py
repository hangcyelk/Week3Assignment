""" this function is ok, it checks if there is any lowercase
in the input string"""


def any_lowercase1(s):
    for c in s:
        if c.islower():
            return True
        else:
            return False

""" this fucntion is not good, first you dont need to
change the c to string cause it already is, second we shall
not consider True or False in python as string but as 
boolean values"""

def any_lowercase2(s):
    for c in s:
        if 'c'.islower():
            return 'True'
        else:
            return 'False'

""" this fucntion will not work because the return fucntion is 
not included in the loop, it will always check the last character"""

def any_lowercase3(s):
    for c in s:
        flag = c.islower()
    return flag

"""this fuction will work, a or gate will return True if any of 
the two boolean value is True"""

def any_lowercase4(s):
    flag = False
    for c in s:
        flag = flag or c.islower()
    return flag

"""this fucntion will not work, because if the first character
is uppercase, it will always return False"""

def any_lowercase5(s):
    for c in s:
        if not c.islower():
            return False
    return True