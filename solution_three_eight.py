def solve_odometer():
	odometer = []
	for i in range(100000,999996):
		if str(i)[-4:]==str(i)[-4:][::-1] and str(i+1)[-5:]==str(i+1)[-5:][::-1] and str(i+2)[1:5] == str(i+2)[1:5][::-1] and str(i+3) == str(i+3)[::-1]:
			odometer.append(int(i))
		else:
			continue
	return odometer

print (solve_odometer())