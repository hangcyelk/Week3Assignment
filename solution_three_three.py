import math

def estimate_pi():
	epsilon = 1e-15
	term = 1
	summation = 0   
	k = 0 
	while term >= epsilon:
		term = (math.factorial(4*k)*(1103+26390*k))/((math.factorial(k))**4*396**(4*k))
		summation += term
		k += 1
	approx = ((2*math.sqrt(2))/9801)*summation
	return 1/approx

print (estimate_pi())