import math

def square_root(num):
	epsilon = 0.0000001
	root = 4
	while True:
		x = root
		root = (x+num/x)/2
		if abs(root-x)<epsilon:
			break
	return root 

def test_square_root():
	epsilon = 0.0000001
	root = 4
	for i in range(1,10):
		num = i
		squareR = square_root(num)
		squarePy = math.sqrt(i)
		Diff = abs(squareR - squarePy)
		print (num, ' ', squareR, ' ', squarePy, ' ', Diff, "\n")

test_square_root()